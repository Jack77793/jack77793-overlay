# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

# Autogenerated by pycargoebuild 0.13.3

EAPI=8

CRATES="
	ab_glyph@0.2.28
	ab_glyph_rasterizer@0.1.8
	adler@1.0.2
	adler2@2.0.0
	ahash@0.8.11
	aho-corasick@1.1.3
	allocator-api2@0.2.18
	android-activity@0.6.0
	android-properties@0.2.2
	android-tzdata@0.1.1
	android_system_properties@0.1.5
	anyhow@1.0.86
	arrayref@0.3.8
	arrayvec@0.7.4
	as-raw-xcb-connection@1.0.1
	ash@0.38.0+1.3.281
	atomic-waker@1.1.2
	autocfg@1.3.0
	bit-set@0.6.0
	bit-vec@0.7.0
	bitflags@1.3.2
	bitflags@2.6.0
	block2@0.5.1
	block@0.1.6
	bumpalo@3.16.0
	bytemuck@1.16.3
	bytemuck_derive@1.7.0
	byteorder@1.5.0
	bytes@1.7.0
	calloop-wayland-source@0.3.0
	calloop@0.13.0
	cc@1.1.7
	cesu8@1.1.0
	cfg-if@1.0.0
	cfg_aliases@0.1.1
	cfg_aliases@0.2.1
	chrono@0.4.38
	codespan-reporting@0.11.1
	com@0.6.0
	com_macros@0.6.0
	com_macros_support@0.6.0
	combine@4.6.7
	concurrent-queue@2.5.0
	core-foundation-sys@0.8.6
	core-foundation@0.9.4
	core-graphics-types@0.1.3
	core-graphics@0.23.2
	core-text@20.1.0
	crc32fast@1.4.2
	crossbeam-utils@0.8.20
	csv-core@0.1.11
	csv@1.3.0
	cursor-icon@1.1.0
	d3d12@22.0.0
	dirs-next@2.0.0
	dirs-sys-next@0.1.2
	dirs-sys@0.4.1
	dirs@5.0.1
	dispatch@0.2.0
	dlib@0.5.2
	document-features@0.2.10
	downcast-rs@1.2.1
	dpi@0.1.1
	dwrote@0.11.0
	encode_unicode@1.0.0
	equivalent@1.0.1
	errno@0.3.9
	fdeflate@0.3.4
	flate2@1.0.30
	flexi_logger@0.28.5
	float-ord@0.3.2
	font-kit@0.14.1
	font-types@0.6.0
	foreign-types-macros@0.2.3
	foreign-types-shared@0.3.1
	foreign-types@0.5.0
	freetype-sys@0.20.1
	gethostname@0.4.3
	getrandom@0.2.15
	gl_generator@0.14.0
	glob@0.3.1
	glow@0.13.1
	glutin_wgl_sys@0.6.0
	gpu-alloc-types@0.3.0
	gpu-alloc@0.6.0
	gpu-allocator@0.26.0
	gpu-descriptor-types@0.2.0
	gpu-descriptor@0.3.0
	hashbrown@0.14.5
	hashbrown@0.15.1
	hassle-rs@0.11.0
	hermit-abi@0.3.9
	hermit-abi@0.4.0
	hexf-parse@0.2.1
	iana-time-zone-haiku@0.1.2
	iana-time-zone@0.1.60
	indexmap@2.3.0
	is-terminal@0.4.12
	itoa@1.0.11
	jni-sys@0.3.0
	jni@0.21.1
	jobserver@0.1.32
	js-sys@0.3.69
	khronos-egl@6.0.0
	khronos_api@3.1.0
	lazy_static@1.5.0
	libc@0.2.155
	libloading@0.8.5
	libredox@0.0.2
	libredox@0.1.3
	linux-raw-sys@0.4.14
	litrs@0.4.1
	lock_api@0.4.12
	log@0.4.22
	malloc_buf@0.0.6
	memchr@2.7.4
	memmap2@0.9.4
	metal@0.29.0
	miniz_oxide@0.7.4
	naga@22.0.0
	ndk-context@0.1.1
	ndk-sys@0.5.0+25.2.9519653
	ndk-sys@0.6.0+11769913
	ndk@0.9.0
	nu-ansi-term@0.50.1
	num-traits@0.2.19
	num_enum@0.7.3
	num_enum_derive@0.7.3
	objc-sys@0.3.5
	objc2-app-kit@0.2.2
	objc2-cloud-kit@0.2.2
	objc2-contacts@0.2.2
	objc2-core-data@0.2.2
	objc2-core-image@0.2.2
	objc2-core-location@0.2.2
	objc2-encode@4.0.3
	objc2-foundation@0.2.2
	objc2-link-presentation@0.2.2
	objc2-metal@0.2.2
	objc2-quartz-core@0.2.2
	objc2-symbols@0.2.2
	objc2-ui-kit@0.2.2
	objc2-uniform-type-identifiers@0.2.2
	objc2-user-notifications@0.2.2
	objc2@0.5.2
	objc@0.2.7
	once_cell@1.19.0
	option-ext@0.2.0
	orbclient@0.3.47
	owned_ttf_parser@0.24.0
	packed-char@0.1.1
	parking_lot@0.12.3
	parking_lot_core@0.9.10
	paste@1.0.15
	pathfinder_geometry@0.5.1
	pathfinder_simd@0.5.4
	percent-encoding@2.3.1
	pin-project-internal@1.1.5
	pin-project-lite@0.2.14
	pin-project@1.1.5
	pkg-config@0.3.30
	png@0.17.13
	polling@3.7.2
	pollster@0.3.0
	presser@0.3.1
	prettytable-rs@0.10.0
	proc-macro-crate@3.1.0
	proc-macro2@1.0.86
	profiling@1.0.15
	quick-xml@0.34.0
	quote@1.0.36
	range-alloc@0.1.3
	raw-window-handle@0.5.2
	raw-window-handle@0.6.2
	read-fonts@0.20.0
	redox_syscall@0.4.1
	redox_syscall@0.5.3
	redox_users@0.4.5
	regex-automata@0.4.7
	regex-syntax@0.8.4
	regex@1.10.5
	renderdoc-sys@1.1.0
	rmp@0.8.14
	rmpv@1.3.0
	rustc-hash@1.1.0
	rustc_version@0.4.0
	rustix@0.38.34
	rustversion@1.0.17
	ryu@1.0.18
	same-file@1.0.6
	scoped-tls@1.0.1
	scopeguard@1.2.0
	sctk-adwaita@0.10.1
	semver@1.0.23
	serde@1.0.204
	serde_derive@1.0.204
	shlex@1.3.0
	simd-adler32@0.3.7
	skrifa@0.20.0
	slab@0.4.9
	slotmap@1.0.7
	smallvec@1.13.2
	smithay-client-toolkit@0.19.2
	smol_str@0.2.2
	spirv@0.3.0+sdk-1.3.268.0
	static_assertions@1.1.0
	strict-num@0.1.1
	swash@0.1.18
	syn@1.0.109
	syn@2.0.72
	term@0.7.0
	termcolor@1.4.1
	thiserror-impl@1.0.63
	thiserror@1.0.63
	tiny-skia-path@0.11.4
	tiny-skia@0.11.4
	toml_datetime@0.6.8
	toml_edit@0.21.1
	tracing-core@0.1.32
	tracing@0.1.40
	ttf-parser@0.24.0
	unicode-ident@1.0.12
	unicode-segmentation@1.11.0
	unicode-width@0.1.13
	unicode-xid@0.2.4
	version_check@0.9.5
	walkdir@2.5.0
	wasi@0.11.0+wasi-snapshot-preview1
	wasm-bindgen-backend@0.2.92
	wasm-bindgen-futures@0.4.42
	wasm-bindgen-macro-support@0.2.92
	wasm-bindgen-macro@0.2.92
	wasm-bindgen-shared@0.2.92
	wasm-bindgen@0.2.92
	wayland-backend@0.3.6
	wayland-client@0.31.5
	wayland-csd-frame@0.3.0
	wayland-cursor@0.31.5
	wayland-protocols-plasma@0.3.3
	wayland-protocols-wlr@0.3.3
	wayland-protocols@0.32.3
	wayland-scanner@0.31.4
	wayland-sys@0.31.4
	web-sys@0.3.69
	web-time@1.1.0
	wgpu-core@22.0.0
	wgpu-hal@22.0.0
	wgpu-types@22.0.0
	wgpu@22.0.0
	widestring@1.1.0
	winapi-i686-pc-windows-gnu@0.4.0
	winapi-util@0.1.8
	winapi-x86_64-pc-windows-gnu@0.4.0
	winapi@0.3.9
	windows-core@0.52.0
	windows-core@0.58.0
	windows-implement@0.58.0
	windows-interface@0.58.0
	windows-result@0.2.0
	windows-strings@0.1.0
	windows-sys@0.45.0
	windows-sys@0.48.0
	windows-sys@0.52.0
	windows-sys@0.59.0
	windows-targets@0.42.2
	windows-targets@0.48.5
	windows-targets@0.52.6
	windows@0.52.0
	windows_aarch64_gnullvm@0.42.2
	windows_aarch64_gnullvm@0.48.5
	windows_aarch64_gnullvm@0.52.6
	windows_aarch64_msvc@0.42.2
	windows_aarch64_msvc@0.48.5
	windows_aarch64_msvc@0.52.6
	windows_i686_gnu@0.42.2
	windows_i686_gnu@0.48.5
	windows_i686_gnu@0.52.6
	windows_i686_gnullvm@0.52.6
	windows_i686_msvc@0.42.2
	windows_i686_msvc@0.48.5
	windows_i686_msvc@0.52.6
	windows_x86_64_gnu@0.42.2
	windows_x86_64_gnu@0.48.5
	windows_x86_64_gnu@0.52.6
	windows_x86_64_gnullvm@0.42.2
	windows_x86_64_gnullvm@0.48.5
	windows_x86_64_gnullvm@0.52.6
	windows_x86_64_msvc@0.42.2
	windows_x86_64_msvc@0.48.5
	windows_x86_64_msvc@0.52.6
	winit@0.30.4
	winnow@0.5.40
	wio@0.2.2
	x11-dl@2.21.0
	x11rb-protocol@0.13.1
	x11rb@0.13.1
	xcursor@0.3.6
	xkbcommon-dl@0.4.2
	xkeysym@0.2.1
	xml-rs@0.8.20
	yazi@0.1.6
	yeslogic-fontconfig-sys@6.0.0
	zeno@0.2.3
	zerocopy-derive@0.7.35
	zerocopy@0.7.35
"

inherit cargo

DESCRIPTION="A WebGPU rendered Neovim GUI"
HOMEPAGE="https://github.com/tim-harding/neophyte"
SRC_URI="
	https://github.com/tim-harding/neophyte/archive/${PV}.tar.gz -> ${P}.tar.gz
	${CARGO_CRATE_URIS}
"

LICENSE="MIT"
# Dependent crate licenses
LICENSE+="
	Apache-2.0 BSD-2 BSD CC0-1.0 ISC MIT MPL-2.0 Unicode-DFS-2016 ZLIB
"
SLOT="0"
KEYWORDS="~amd64"
